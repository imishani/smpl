# Post Processing

---

## Introduction

This document describes the post processing utilities SMPL provides. 

First, we have 3 main **classes** which in part being utilized by the upcoming functions:

1. `JointPositionShortcutPathGenerator`
2. `JointPositionVelocityShortcutPathGenerator`
3. `EuclidShortcutPathGenerator`

Now we can move on to the **functions**:
* `ShortcutPath`
* `InterpolatePath`
* `ExtractPositionPath`
* `CreatePositionVelocityPath`
* `ComputePositionPathCosts`
* `ComputePositionVelocityPathCosts`

## Usage

---
TODO