# SMPL_TEST

This directory contains the SMPL test suite. 
To run the test you need to have all the following packages installed:
- [smpl]()
- [smpl_ros]()
- [smpl_ompl_interface]()
- [sbpl]()
- [sbpl_collision_checking]()
- [sbpl_collision_checking_test]()
- [smpl_moveit_interface]()
